
import points
import math

def get_a(T):
	a1 = (T[1]['y'] - T[0]['y']) * (T[2]['z'] - T[0]['z']) 
	a2 = (T[1]['z'] - T[0]['z']) * (T[2]['y'] - T[0]['z'])
	a = a1 - a2
	return a

def get_b(T):
	b1 = (T[1]['z'] - T[0]['z']) * (T[2]['x'] - T[0]['x']) 
	b2 = (T[1]['x'] - T[0]['x']) * (T[2]['z'] - T[0]['z'])
	b = b1 - b2
	return b

def get_c(T):
	b1 = (T[1]['x'] - T[0]['x']) * (T[2]['y'] - T[0]['y']) 
	b2 = (T[1]['y'] - T[0]['y']) * (T[2]['x'] - T[0]['x'])
	b = b1 - b2
	return b

def get_lambda(a, b, c):
	return math.sqrt(a**2 + b**2 + c**2)

def get_n(T):
	a = get_a(T)
	b = get_b(T)
	c = get_c(T)
	l = get_lambda(a,b,c)
	return {'x' : a/l, 'y' : b / l, 'z' : c / l}

def get_beta(T):
	dx2 = (T[1]['x'] - T[0]['x'])**2 
	dy2 = (T[1]['y'] - T[0]['y'])**2 
	dz2 = (T[1]['z'] - T[0]['z'])**2 
	return math.sqrt(dx2 + dy2 + dz2)

def get_ksi(T):
	dx = (T[1]['x'] - T[0]['x']) 
	dy = (T[1]['y'] - T[0]['y']) 
	dz = (T[1]['z'] - T[0]['z']) 
	b = get_beta(T)
	return {'x' : dx / b, 'y' : dy / b, 'z' : dz / b}

def get_eta(ksi, n):
	eta_x = n['y'] * ksi['z'] - ksi['y'] * n['z']
	eta_y = n['z'] * ksi['x'] - ksi['z'] * n['x']
	eta_z = n['x'] * ksi['y'] - ksi['x'] * n['y']

	# eta_x = ksi['y'] * n['z'] - n['y'] * ksi['z']
	# eta_y = ksi['z'] * n['x'] - n['z'] * ksi['x']
	# eta_z = ksi['x'] * n['y'] - n['x'] * ksi['y']
	return {'x' : eta_x, 'y' : eta_y, 'z' : eta_z}

def ww(T):
	dx = (T[1]['x'] - T[0]['x']) / 2.0
	dy = (T[1]['y'] - T[0]['y']) / 2.0 
	dz = (T[1]['z'] - T[0]['z']) / 2.0 
	# return {'x' : dx, 'y' : dy, 'z' : dz}
	return {'x' : 0.0, 'y' : 0.0, 'z' : 0.0}

def get_val_i(vec, w, ti):
	xx = vec['x'] * (ti['x'] - w['x']) 
	yy = vec['y'] * (ti['y'] - w['y'])
	zz = vec['z'] * (ti['z'] - w['z'])
	return xx + yy + zz
	# return [xx + yy + zz, xx, yy, zz]

def get_all_for_points(T, mess):
	n = get_n(T)
	ksi = get_ksi(T)
	eta = get_eta(ksi, n)
	w = ww(T)
	ksi_1 = get_val_i(ksi, w, T[0])
	# [ksi_1, xx, yy, zz] = get_val_i(ksi, w, points.T[0])
	# print ('xx = {}'.format(xx))
	# print ('yy = {}'.format(yy))
	# print ('zz = {}'.format(zz))
	ksi_2 = get_val_i(ksi, w, T[1])
	ksi_3 = get_val_i(ksi, w, T[2])

	eta_1 = get_val_i(eta, w, T[0])
	eta_2 = get_val_i(eta, w, T[1])
	eta_3 = get_val_i(eta, w, T[2])

	print ('=============={}==========='.format(mess))

	print ('ksi1 = {}'.format(ksi_1))
	print ('ksi2 = {}'.format(ksi_2))
	print ('ksi3 = {}'.format(ksi_3))

	print ('eta1 = {}'.format(eta_1))
	print ('eta2 = {}'.format(eta_2))
	print ('eta3 = {}'.format(eta_3))

	print ('w = {}'.format(w))
	print ('n = {}'.format(n))
	print ('ksi = {}'.format(ksi))
	print ('eta = {}'.format(eta))
	
	print ('A:: p1 = {}, p2 = {}, p3 = {}'.format((eta_3 - eta_2), (eta_1 - eta_3), (eta_2 - eta_1)))
	print ('B:: p1 = {}, p2 = {}, p3 = {}'.format((ksi_2 - ksi_3), (ksi_3 - ksi_1), (ksi_1 - ksi_2)))
	print ('C:: p1 = {}, p2 = {}, p3 = {}'.format(0.0, 0.0, 0.0))


def main():
	get_all_for_points(points.VT1, 'vt1')
	get_all_for_points(points.VT2, 'vt2')


if __name__ == '__main__':
    main()


